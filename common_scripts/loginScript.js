 var Urls = require('./urls');

exports.loginfunction = function (casper, username, password) {
 
    casper.start(Urls.loginUrl, function () {
        //casper.echo(casper.getPageContent())
    });

    casper.waitForSelector('#signIn-Form', function () {
        var self = this;
        casper.echo("Autofill testing -------->>>>")
        self.fill('#signIn-Form', {
            'username': username,
            'password': password

        }, false);
    });

    casper.then(function () {
        var self = this;
        self.click(".signInBtn");
    });
 

}