casper.options.viewportSize = { width: 1280, height: 1200 }
var login = require("../../common_scripts/loginScript.js")
var timeFormatFunction = require("../../common_scripts/timeFormat.js")
var x = require('casper').selectXPath;
login.loginfunction(casper, 'pramod.dassanayake@eyepax.com.lk', 'abc123');
var Urls = require('../../common_scripts/urls');


//==========================================================================Test 1- Event List testing JITO================================================================ 
casper.test.begin('Event List testing JITO', 7, function (test) {



    casper.waitForSelector('.event-list-head', function () {
        var self = this
        casper.echo("arrived");
        //  self.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "Page_loaded.png");
        test.assertUrlMatch(self.getCurrentUrl(), Urls.eventListUrl);

    });

    casper.waitForSelector('.list-group', function () {
        var self = this
        //  self.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_2.png");
        test.pass("The list-group item has loaded")
        // test.assertUrlMatch(self.getCurrentUrl(), 'http://localhost:4200/eventlist');
        casper.waitForSelector('.list-group-item', function () {
            test.pass("First Element in the list is loaded")
            // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_3.png");
        }, function () {
            test.fail("First Element in the list did not load")
            // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_4.png");
        });




    }, function () {
        test.fail("Event List did not load")
        //this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_5.png");
    });

    casper.then(function () {
        var listItems = casper.getElementsInfo('.list-group-item');
        casper.echo("length of the items in the list"+listItems.length);
        test.assertEqual(listItems.length, 20, "The count of the items are correct");
        // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_6.png");
    })
    casper.wait(5000)
    casper.then(function () {
        var listItem_title = casper.getElementInfo('.list-group-item-heading').text;
        var listItems_location = casper.getElementInfo('.list-group-item-text').text;
        //var listItem_Time = casper.getElementInfo(x('/html/body/app-root/app-eventlist/div/div[2]/div[2]/div/div[2]/p[2]')).text;
        //casper.echo(listItem_Time);
        //this.capture("screenshots_EventList_Loaded/" +timeFormatFunction.timeFormatFucntion() + "screenshot_7.png");

        test.assertEqual(listItem_title, "Charly & the Angless", "The title is correct");
        test.assertEqual(listItems_location, "Benguela", "_Test item checked");

    })

    casper.then(function () {
        casper.evaluate(function () {
            document.getElementsByClassName('list-group-item')[18].scrollIntoView();
        });
    });


    casper.wait(1000)

    casper.then(function () {
        var listItems = casper.getElementsInfo('.list-group-item');
        if (listItems.length > 20) {
            test.pass("Pagination Occurred")
        }
        else {
            test.fail("Pagination didn't occurr")
        }
    });

    casper.then(function () {
        this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "scrolltest.png");
    })




    casper.test.on("fail", function (failure) {
        casper.capture('screenshots_EventList_Loaded/' + timeFormatFunction.timeFormatFucntion() + failure.message.replace(/[^a-z0-9]/gi, '_').toLowerCase() + '.png');
    });

    casper.run(function () {
        test.done();
    })

});

// ========================================================================Test 2 -EventList Cancel  button testing=================================================================
casper.test.begin('EventList Cancel  button testing', 1, function (test) {

    casper.waitForSelector('.list-group', function () {
        casper.evaluate(function () {
            document.getElementById('86').click();
            //document.getElementById('c').= true;
        });
    });
    casper.then(function () {
        var self = this;
        self.click(x('//*[@id="cancek-button"]'));
    })

    casper.then(function () {
        var isChecked = casper.evaluate(function () {
            return document.getElementById('86').checked;
            //document.getElementById('c').= true;

        });

        test.assertEqual(isChecked, false, "The radio button check value is " + isChecked)
    });

    casper.test.on("fail", function (failure) {
        casper.capture('screenshots_EventList_Loaded/' + timeFormatFunction.timeFormatFucntion() + failure.message.replace(/[^a-z0-9]/gi, '_').toLowerCase() + '.png');
    });

    casper.run(function () {
        test.done();
    });


});

//=============================================================================Test 3- EventList Done  button testing==================================================================================

casper.test.begin('EventList Done  button testing', 1, function (test) {


    casper.waitForSelector('.list-group', function () {
        casper.evaluate(function () {
            document.getElementById('86').click();
            //document.getElementById('c').= true;
        });
    });


    casper.then(function () {
        this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClick.png");
    })

    casper.then(function () {
        var self = this;
        self.click(x('//*[@id="done-button"]'));
        casper.echo("clicked========>>>>.");
        // casper.wait(2000, function () {
        //     this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClickAfter.png");

        // })

    });

    casper.waitFor(function check() {

        this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClickNextPage.png");
        console.log('*' + this.getCurrentUrl());
        return this.getCurrentUrl() === "http://localhost:8080/";
    }, function then() {

        test.pass("Atendee list navigation success\n\n");
    });

    casper.then(function () {
        casper.wait(5000, function () {
            this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClickNextPage.png");
            // casper.echo(casper.getPageContent());
        })
    });


    casper.then(function () {
        casper.start("http://localhost:8080/");
        // casper.echo(casper.getPageContent());
        casper.then(function () {
            casper.wait(2000, function () {
                this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "afterloadingAttendeesPage.png");
            })
        })
    })


    // casper.on('page.error', function (msg, trace) {
    //     this.echo('Error.log : ' + msg, 'ERROR');
    // });

    // casper.waitForSelector('',function(){

    // });
    // casper.on("remote.message", function (message) {
    //     this.echo("remote console.log: " + message);
    // });

    casper.test.on("fail", function (failure) {
        casper.capture('screenshots_EventList_Loaded/' + timeFormatFunction.timeFormatFucntion() + failure.message.replace(/[^a-z0-9]/gi, '_').toLowerCase() + '.png');
    });


    casper.run(function () {
        test.done();
    })
});









