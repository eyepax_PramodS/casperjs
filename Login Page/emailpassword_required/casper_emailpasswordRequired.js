casper.options.viewportSize = { width: 1280, height: 1200 }
casper.test.begin('Sign In Jito with no email and password entered', 1, function (test) {
    var x = require('casper').selectXPath;


    casper.start('http://localhost:4200/login', function () {
        casper.echo(casper.getPageContent())
    });


    casper.waitForSelector('#signIn-Form', function () {
        var self = this;
        // casper.echo("Autofill testing -------->>>>")
        self.capture("screenshots_email_password_required/screenshot_1.png");

    });

    casper.then(function () {
        var self = this;
        self.capture("screenshots_email_password_required/screenshot_2.png");
        casper.echo("button click testing -------->>>>")
        self.click(".signInBtn");
        self.capture("screenshots_email_password_required/screenshot_3.png");
    });

    //wait for the alert messgae and check the content
    casper.waitForAlert(function (response) {
        var self = this;
        self.capture("screenshots_email_password_required/screenshot_4.png");
        self.echo("Alert received: " + response.data);
        self.test.assertMatch(response.data, /The email field is required. and The password field is required./);

    });
    casper.run(function () {
        var self = this;
        self.capture("screenshots_email_password_required/screenshot_5.png");
        test.done();
    });
});