casper.options.viewportSize = { width: 1280, height: 1200 }
casper.test.begin('Sign In Jito with incorrect credentials', 1, function (test) {
var x = require('casper').selectXPath;


    casper.start('http://localhost:4200/login', function () {
        casper.echo(casper.getPageContent())
    });


    casper.waitForSelector('#signIn-Form', function () {
        var self = this;
        casper.echo("Autofill testing -------->>>>")
        self.capture("screenshots_incorrect_credentials/screenshot_1.png");
        self.fill('#signIn-Form', {
            'username': 'pramod.dassanayake@eyepax.com.lk',
            'password': 'wrongPassword'

        }, false);
        self.capture("screenshots_incorrect_credentials/screenshot_2.png");
    });


    casper.then(function () {
        var self = this;
        self.capture("screenshots_incorrect_credentials/screenshot_3.png");
        casper.echo("button click testing -------->>>>")
        self.click(".signInBtn");
        self.capture("screenshots_incorrect_credentials/screenshot_4.png");
    });

// checking whether the credentials are correct
     casper.waitForAlert(function (response) {
        var self = this;
        self.capture("screenshots_incorrect_credentials/screenshot_6.png");
        self.echo("Alert received: " + response.data);
        self.test.assertMatch(response.data, /Invalid email or password./);

    });

  
    casper.run(function () {
        var self = this;
        self.capture("screenshots_incorrect_credentials/screenshot_8.png");
        test.done();
    });
});