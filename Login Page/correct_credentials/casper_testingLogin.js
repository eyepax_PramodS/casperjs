//var casper = require('casper').create();
casper.options.viewportSize = { width: 1280, height: 800 }
casper.test.begin('Sign In Jito', 1, function (test) {

    // Directing to the login page
    
    casper.start('http://localhost:4200/login', function () {
        casper.echo(casper.getPageContent())
    });

    // Autofilling the Login page inputs

    casper.waitForSelector('#signIn-Form', function () {
        var self = this;
        casper.echo("Autofill testing -------->>>>")
        self.capture("screenshots_correct_credentials/screenshot_1.png");
        self.fill('#signIn-Form', {
            'username': 'pramod.dassanayake@eyepax.com.lk',
            'password': 'abc123'

        }, false);
        self.capture("screenshots_correct_credentials/screenshot_2.png");
    });


    // Log in button click 

    casper.then(function () {
        var self = this;
        self.capture("screenshots_correct_credentials/screenshot_3.png");
        casper.echo("button click testing -------->>>>")
        self.click(".signInBtn");
        self.capture("screenshots_correct_credentials/screenshot_4.png");
    });

// Checking whether directed to the correct URL

    casper.waitForSelector('.event-list-head', function () {
        var self = this;
        self.capture("screenshots_correct_credentials/screenshot_5.png");
        test.assertUrlMatch(self.getCurrentUrl(), 'http://localhost:4200/eventlist');
        console.log("current Url------------>>>>>>>" + this.getCurrentUrl())
        self.capture("screenshots_correct_credentials/screenshot_6.png");
    });

    casper.run(function () {
        var self = this;
        self.capture("screenshots_correct_credentials/screenshot_7.png");
        test.done();
    });
});