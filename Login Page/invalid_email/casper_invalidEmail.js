casper.options.viewportSize = { width: 1280, height: 1200 }
casper.test.begin('Sign In Jito with a invalid email address', 1, function (test) {
    var x = require('casper').selectXPath;


    casper.start('http://localhost:4200/login', function () {
        casper.echo(casper.getPageContent())
    });


    casper.waitForSelector('#signIn-Form', function () {
        var self = this;
        // casper.echo("Autofill testing -------->>>>")
         self.fill('#signIn-Form', {
            'username': 'pramod.@eyepax.com.lk',
            'password': 'abc123'

        }, false);
        self.capture("screenshots_invalid_email/screenshot_1.png");

    });
    casper.then(function () {
        var self = this;
        self.capture("screenshots_invalid_email/screenshot_2.png");
        casper.echo("button click testing -------->>>>")
        self.click(".signInBtn");
        self.capture("screenshots_invalid_email/screenshot_3.png");
    });
    casper.waitForAlert(function (response) {
        var self = this;
        self.capture("screenshots_invalid_email/screenshot_4.png");
        self.echo("Alert received: " + response.data);
        self.test.assertMatch(response.data, /The email must be a valid email address./);

    });
    casper.run(function () {
        var self = this;
        self.capture("screenshots_invalid_email/screenshot_5.png");
        test.done();
    });
});