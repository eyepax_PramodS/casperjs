casper.options.viewportSize = { width: 1280, height: 1200 }
var login = require("../../common_scripts/loginScript.js")
var timeFormatFunction = require("../../common_scripts/timeFormat.js")
var x = require('casper').selectXPath;
login.loginfunction(casper, 'pramod.dassanayake@eyepax.com.lk', 'abc123');
var initialItemList = 0;
var paginatedItemList = 0;

var initialPendingList = 0;
var PaginatedPendingList = 0;

//================================================================================= Attendees List Navigation==========================================================
casper.test.begin('Attendees List Testing Arrived List', 5, function (test) {

    casper.wait(2000)
    casper.waitForSelector('.list-group-item', function () {
        casper.evaluate(function () {
            document.getElementsByClassName('list-group-item')[18].scrollIntoView();
        });;
    });

    casper.then(function () {
        casper.evaluate(function () {
            document.getElementById('86').click();
        });
    });


    casper.then(function () {
        //this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClick.png");
    })

    casper.then(function () {
        var self = this;
        self.click(x('//*[@id="done-button"]'));
        casper.echo("clicked========>>>>.");
    });

    casper.waitFor(function check() {

        // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClickNextPage.png");
        this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "checkAttendeesList.png");
        console.log('*' + this.getCurrentUrl());
        return this.getCurrentUrl() === "http://localhost:8080/";
    }, function then() {

        test.pass("Atendee list navigation success\n\n");
    });

    //============================================================ Attendees List Testing Arrived List=========================================================

    casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[1]/h1'), function () {

        this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "checkAttendeesList_arrived.png");
        //casper.echo("Nav bar======================>")
        test.pass("Nav bar Loaded");
    });

    casper.waitForSelector(x('//*[@id="noanim-tab-example-tab-2"]'), function () {
        this.click(x('//*[@id="noanim-tab-example-tab-2"]'))

    })

    casper.wait(1000, function () {

    })

    casper.waitForSelector('.arrived', function () {
        casper.wait(2000);
        this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "checkAttendeesList_tab_arrived.png");
        //casper.echo("The Guest List loaded")
        test.pass("The Guest List Loaded");
    });

    casper.then(function () {
        var listItems = casper.getElementsInfo('.arrived');
        casper.echo("length of the arrived list=====>"+listItems.length);
        test.assertEqual(listItems.length, 20, "The count of the items are correct" + listItems.length);
        // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_6.png");
    })

    casper.then(function () {
        initialItemList = casper.getElementsInfo('.arrived')
        casper.evaluate(function () {
            //initialItemList=document.getElementsByClassName('list-group-item').length
            document.getElementsByClassName('arrived')[19].scrollIntoView();
        });

    });

    casper.wait(5000, function () {
        this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "AttendeesList_scrollView_arrived.png");
    })
    casper.then(function () {
        paginatedItemList = casper.getElementsInfo('.arrived')
        casper.echo("paginated list length ::" + paginatedItemList.length)
        if (paginatedItemList.length > initialItemList.length) {
            test.pass("The Atendees list pagination is functioning corretly")
        }
        else {
            test.fail("The Atendees list pagination is not functioning corretly")
        }
    })

     casper.test.on("fail", function (failure) {
        casper.capture('screenshots_Attendess_list/' + timeFormatFunction.timeFormatFucntion() + failure.message.replace(/[^a-z0-9]/gi, '_').toLowerCase() + '.png');
    });
 
    // casper.on('page.error', function (msg, trace) {
    //     this.echo('Error.log : ' + msg, 'ERROR');
    // });

    // // casper.waitForSelector('',function(){

    // // });
    // casper.on("remote.message", function (message) {
    //     this.echo("remote console.log: " + message);
    // });
    casper.run(function () {
        test.done();
    });

});


//=================================================================  Attendees List Testing Pending List =================================================

casper.test.begin('Attendees List Testing Pending List', 2, function (test) {



    casper.waitForSelector(x('//*[@id="noanim-tab-example-tab-1"]'), function () {
        this.click(x('//*[@id="noanim-tab-example-tab-1"]'))

    })

    casper.waitForSelector('.Pending', function () {
        var listItems = casper.getElementsInfo('.Pending');
        casper.echo(listItems.length);
        test.assertEqual(listItems.length, 20, "The count of the items are correct " + listItems.length);
        casper.wait(1000, function () {
            this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "checkAttendeesList_pending.png");

        })

    });

    // casper.then(function () {
    //     var listItems = casper.getElementsInfo('.Pending');
    //     casper.echo(listItems.length);
    //     test.assertEqual(listItems.length, 20, "The count of the items are correct "+listItems.length);
    //     // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "screenshot_6.png");
    // })

    casper.waitForSelector('.Pending', function () {
        initialPendingList = casper.getElementsInfo('.Pending')
        casper.evaluate(function () {
            //initialItemList=document.getElementsByClassName('list-group-item').length
            document.getElementsByClassName('Pending')[19].scrollIntoView();
        });

    });

    casper.wait(5000, function () {
        this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "AttendeesList_scrollView_pending.png");
    })
    casper.then(function () {
        PaginatedPendingList = casper.getElementsInfo('.Pending')
        casper.echo("paginated list length ::" + PaginatedPendingList.length)
        if (PaginatedPendingList.length > initialPendingList.length) {
            test.pass("The Atendees list Pending pagination is functioning corretly")
        }
        else {
            test.fail("The Atendees list Pending pagination is not functioning corretly")
        }
    })


    
     casper.test.on("fail", function (failure) {
        casper.capture('screenshots_Attendess_list/' + timeFormatFunction.timeFormatFucntion() + failure.message.replace(/[^a-z0-9]/gi, '_').toLowerCase() + '.png');
    });

    // casper.on('page.error', function (msg, trace) {
    //     this.echo('Error.log : ' + msg, 'ERROR');
    // });


    // casper.on("remote.message", function (message) {
    //     this.echo("remote console.log: " + message);
    // });
    casper.run(function () {
        test.done();
    });

});

//=================================================================  Attendees List Testing Search functionality =================================================

// casper.test.begin('Attendees List Testing Pending List Search Functionlaity Testing', 1, function (test) {

//     casper.waitForSelector(x('//*[@id="app"]/div/div[2]/div/div[1]/input'), function () {
//         var self = this;
//         self.fill(x('//*[@id="app"]/div/div[2]/div/div[1]'), {
//             'Search': 'pramod'
//         }, false)
//     })

//     casper.then(function () {
//         this.click(x('//*[@id="app"]/div/div[2]/div/div[1]/button'))
//     });

//     casper.then(function () {
//         casper.wait(5000, function () {
//             this.capture("screenshots_Attendess_list/" + timeFormatFunction.timeFormatFucntion() + "checkAttendeesList_Search.png");
//         });
//     })




//     casper.on('page.error', function (msg, trace) {
//         this.echo('Error.log : ' + msg, 'ERROR');
//     });


//     casper.on("remote.message", function (message) {
//         this.echo("remote console.log: " + message);
//     });
//     casper.run(function () {
//         test.done();
//     });


// });