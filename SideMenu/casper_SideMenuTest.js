casper.options.viewportSize = { width: 1280, height: 1200 }
var login = require("../../common_scripts/loginScript.js")
var getAttendees = require("../../common_scripts/AttendeesScript.js");
var timeFormatFunction = require("../../common_scripts/timeFormat.js");
var searchFunction = require("../../common_scripts/searchFunction.js")
var x = require('casper').selectXPath;

casper.test.begin('Side Menu Testing', 2, function (test) {



    login.loginfunction(casper, 'pramod.dassanayake@eyepax.com.lk', 'abc123');

    casper.then(function () {
        getAttendees.getAttendessList(86);
    })

    //  ============================================================ Side Menu Testing change Event =================================================================
    casper.then(function () {
        casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[1]/button[1]'), function () {
            this.click(x('//*[@id="app"]/div/div[1]/div/div/div[1]/button[1]'))
        });
    })
    casper.then(function () {
        casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[2]/div[2]/img'), function () {
            test.pass("The Side menu has loaded...");

        })
    })
    casper.then(function () {
        this.capture("screenshots_SideMenu/" + timeFormatFunction.timeFormatFucntion() + "MenuOpen.png");
    })

    casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[2]/div[2]/div[3]/div/button'), function () {

        this.click(x('//*[@id="app"]/div/div[1]/div/div/div[2]/div[2]/div[3]/div/button'))
    })

    casper.waitFor(function check() {

        // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClickNextPage.png");
        //  this.capture("screenshots_SideMenu/" + timeFormatFunction.timeFormatFucntion() + "checkEventListLoaded.png");
        console.log('*' + this.getCurrentUrl());
        return this.getCurrentUrl() === "http://localhost:4200/eventlist";
    }, function then() {

        test.pass("Event list navigation success\n\n");
    });




    casper.on('page.error', function (msg, trace) {
        this.echo('Error.log : ' + msg, 'ERROR');
    });


    // casper.on("remote.message", function (message) {
    //     this.echo("remote console.log: " + message);
    // });
    casper.run(function () {
        test.done();
    });

});

//  ============================================================ Side Menu Testing Log Out =================================================================

casper.test.begin('Side Menu Testing', 2, function (test) {




    login.loginfunction(casper, 'pramod.dassanayake@eyepax.com.lk', 'abc123');

    casper.then(function () {
        getAttendees.getAttendessList(86);
    })


    casper.then(function () {
        casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[1]/button[1]'), function () {
            this.click(x('//*[@id="app"]/div/div[1]/div/div/div[1]/button[1]'))
        });
    })
    casper.then(function () {
        casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[2]/div[2]/img'), function () {
            test.pass("The Side menu has loaded...");

        })
    })

    casper.waitForSelector(x('//*[@id="app"]/div/div[1]/div/div/div[2]/div[2]/div[4]/div/button'), function () {

        this.click(x('//*[@id="app"]/div/div[1]/div/div/div[2]/div[2]/div[4]/div/button'))
    })

    casper.waitForSelector(x('/html/body/div[2]/div/div[1]/div/div'), function () {
        this.click(x('/html/body/div[2]/div/div[1]/div/div/div[2]/button[2]'))
    })

    casper.waitFor(function check() {

        // this.capture("screenshots_EventList_Loaded/" + timeFormatFunction.timeFormatFucntion() + "eventClickNextPage.png");
        // this.capture("screenshots_SideMenu/" + timeFormatFunction.timeFormatFucntion() + "checkAttendeesList.png");
        console.log('*' + this.getCurrentUrl());
        return this.getCurrentUrl() === "http://localhost:4200/login";
    }, function then() {

        test.pass("Log out Success\n\n");
    });



    casper.on('page.error', function (msg, trace) {
        this.echo('Error.log : ' + msg, 'ERROR');
    });


    // casper.on("remote.message", function (message) {
    //     this.echo("remote console.log: " + message);
    // });
    casper.run(function () {
        test.done();
    });


});